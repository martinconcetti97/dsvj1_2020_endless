#include "raylib.h"

#include "gameLoop.h"

#include "Gameplay/Screen/screen.h"
#include "Gameplay/gameplayHandle.h"
#include "Menu/menuHandeling.h"

namespace Endless {
	namespace GameLoop {

		bool inGame;
		Music soundTrack;

		//----------------------------------------------------
		static void initAll() {
			inGame = true;
			//--------
			InitAudioDevice();

			soundTrack = LoadMusicStream("res/assets/sound/music/ES_A Hoax - Mary Riddle.mp3");
			if (MenuHanlde::onMenu) {
				PlayMusicStream(soundTrack);
			}
			ScreenHandle::init();
			//--------
			MenuHanlde::init();
			//--------
			GameManager::initGameElements();
			//--------
		}
		//----------------------------------------------------
		static void deinitAll() {
			//--------
			GameManager::deinitGameElements();
			//--------
			MenuHanlde::deinit();
			//--------
		}
		//----------------------------------------------------
		static void drawAll() {
			//-------------
			ClearBackground(BLACK);
			BeginDrawing();
			//-------------
			if (MenuHanlde::onMenu) {
				//------------
				MenuHanlde::draw();
				//------------
			}
			else {
				//------------
				GameManager::drawGameElements();
				//------------
			}
			//------------
			EndDrawing();
			//------------
		}
		//----------------------------------------------------
		//static void changeResolution() {
		//	
		//}
		//----------------------------------------------------
		static void update() {

			if (MenuHanlde::onMenu) {
				UpdateMusicStream(soundTrack);
			}
			else {
				StopMusicStream(soundTrack);
			}
			if (GameManager::onGameplay) {
				UpdateMusicStream(soundTrack);
			}
			else {
				StopMusicStream(soundTrack);
			}
			if (MenuHanlde::onMenu) {
				//------------
				MenuHanlde::update();
				//------------
				//changeResolution();
				//------------
			}
			else if (!MenuHanlde::onMenu) {
				//------------
				GameManager::inputsGame();
				//------------
				GameManager::updateGameElements();
				//------------
			}
		}
		//----------------------------------------------------
		void game() {
			//------------
			initAll();
			//------------
			while (inGame && !WindowShouldClose())
			{
				//------------
				update();
				//------------
				drawAll();
				//------------
			}
			//------------
			deinitAll();
			//------------
		}
		//----------------------------------------------------
	}
}