#ifndef SCENE_H
#define SCENE_H

#include "raylib.h"

#include "Gameplay/gameplayHandle.h"
#include "Gameplay/Obstacles/obstacles.h"

namespace Endless {
	namespace SceneManagerGame {
		//------------------------------
		//------------------------------
		struct GAME_THINGS
		{
			//-------------------
			Texture2D backgroundLayer1;
			Texture2D backgroundLayer2;
			Texture2D backgroundLayer3;
			//-------------------
			Rectangle floorLayer0;
			Rectangle floorLayer1;
			Rectangle floorLayer2;
			//-------------------
		};
		extern GAME_THINGS scene;
		//------------------------------
		void init();
		void loadTextures();
		void drawLastFrame();
		void draw();
		void resizeOnResolution();
		void update();
		void updateBgsPos();
		void deinit();
		void unloadTextures();
	}
}
#endif // !SCENE_H