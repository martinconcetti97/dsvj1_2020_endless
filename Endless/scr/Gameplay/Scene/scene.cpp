#include "scene.h"

#include "Gameplay/Screen/screen.h"
#include "Gameplay/Player/player.h"
#include "Gameplay/Obstacles/obstacles.h"

using namespace Endless;
using namespace ScreenHandle;
using namespace Player;

namespace Endless {
	namespace SceneManagerGame {
		//-----------------------------------------
		GAME_THINGS scene;
		//-----------------------------------------
		static float POSFLOORx = 0.0f;
		static float FLOLAYER0y;
		static float FLOLAYER1y;
		static float FLOLAYER2y;
		static float WIDTHFLOOR;
		static float HEIGTHFLOOR;
		//-----------------------------------------
		static Vector2 SCROLLBGLAYER1;
		static Vector2 SCROLLBGLAYER2;
		static Vector2 SCROLLBGLAYER3;

		static Vector2 POSBGLAYER1;
		static Vector2 POSBGLAYER2;
		static Vector2 POSBGLAYER3;
		//-----------------------------------------
		float scrollingBack = 0.0f;
		float scrollingMid = 0.0f;
		float scrollingFore = 0.0f;
		//-----------------------------------------
		void init() {
			//--------------
			FLOLAYER0y = static_cast<float>(toPoseLayer0);
			FLOLAYER1y = static_cast<float>(toPoseLayer1);
			FLOLAYER2y = static_cast<float>(toPoseLayer2);
			WIDTHFLOOR = static_cast<float>(actualWidth);
			HEIGTHFLOOR = 10.0f;
			//--------------
			scene.floorLayer0 = { POSFLOORx,FLOLAYER0y,WIDTHFLOOR,HEIGTHFLOOR };
			scene.floorLayer1 = { POSFLOORx,FLOLAYER1y,WIDTHFLOOR,HEIGTHFLOOR };
			scene.floorLayer2 = { POSFLOORx,FLOLAYER2y,WIDTHFLOOR,HEIGTHFLOOR };
			//--------------
			loadTextures();
			//--------------
			SCROLLBGLAYER1 = { scrollingBack, 0.0f };
			SCROLLBGLAYER2 = { scrollingMid , 0.0f };
			SCROLLBGLAYER3 = { scrollingFore, 0.0f };
			POSBGLAYER1 = { scene.backgroundLayer1.width * 2 + scrollingBack,0.0f };
			POSBGLAYER2 = { scene.backgroundLayer2.width * 2 + scrollingMid ,0.0f };
			POSBGLAYER3 = { scene.backgroundLayer3.width * 2 + scrollingFore,0.0f };
			//--------------
		}
		//-----------------------------------------
		void loadTextures() {
			Image rezise;
			if (!onRezise) {
				//-----------------
				rezise = LoadImage("res/assets/background/bakcgroundLayer0.png");
				ImageResize(&rezise, static_cast<int>(actualWidth / 2), static_cast<int>(actualHeigth / 2));
				scene.backgroundLayer1 = LoadTextureFromImage(rezise);
				UnloadImage(rezise);
				//-----------------
				rezise = LoadImage("res/assets/background/bakcgroundLayer1.png");
				ImageResize(&rezise, static_cast<int>(actualWidth / 2), static_cast<int>(actualHeigth / 2));
				scene.backgroundLayer2 = LoadTextureFromImage(rezise);
				UnloadImage(rezise);
				//-----------------
				rezise = LoadImage("res/assets/background/bakcgroundLayer2.png");
				ImageResize(&rezise, static_cast<int>(actualWidth / 2), static_cast<int>(actualHeigth / 2));
				scene.backgroundLayer3 = LoadTextureFromImage(rezise);
				UnloadImage(rezise);
			}
			else {
				unloadTextures();
				//-----------------
				rezise = LoadImage("res/assets/background/bakcgroundLayer0.png");
				ImageResize(&rezise, static_cast<int>(actualWidth / 2), static_cast<int>(actualHeigth / 2));
				scene.backgroundLayer1 = LoadTextureFromImage(rezise);
				UnloadImage(rezise);
				//-----------------
				rezise = LoadImage("res/assets/background/bakcgroundLayer1.png");
				ImageResize(&rezise, static_cast<int>(actualWidth / 2), static_cast<int>(actualHeigth / 2));
				scene.backgroundLayer2 = LoadTextureFromImage(rezise);
				UnloadImage(rezise);
				//-----------------
				rezise = LoadImage("res/assets/background/bakcgroundLayer2.png");
				ImageResize(&rezise, static_cast<int>(actualWidth / 2), static_cast<int>(actualHeigth / 2));
				scene.backgroundLayer3 = LoadTextureFromImage(rezise);
				UnloadImage(rezise);
			}
		}
		//-----------------------------------------
		void drawLastFrame() {
			//PARALLAX-----------------------
			DrawTextureEx(scene.backgroundLayer1, SCROLLBGLAYER1, 0.0f, 2.0f, WHITE);
			DrawTextureEx(scene.backgroundLayer1, POSBGLAYER1, 0.0f, 2.0f, WHITE);

			DrawTextureEx(scene.backgroundLayer2, SCROLLBGLAYER2, 0.0f, 2.0f, WHITE);
			DrawTextureEx(scene.backgroundLayer2, POSBGLAYER2, 0.0f, 2.0f, WHITE);

			DrawTextureEx(scene.backgroundLayer3, SCROLLBGLAYER3, 0.0f, 2.0f, WHITE);
			DrawTextureEx(scene.backgroundLayer3, POSBGLAYER3, 0.0f, 2.0f, WHITE);
			//OTHER-----------------------
			ObstaclesManager::drawRoofall();
			if (ObstaclesManager::obstacle2.layer != Player::guy.layer) {
				switch (ObstaclesManager::obstacle2.layer)
				{
				case 0:
					ObstaclesManager::draw();
					Player::draw();
					break;
				case 1:
					if (Player::guy.layer == 2) {
						ObstaclesManager::draw();
						Player::draw();
					}
					else if (Player::guy.layer == 0) {
						Player::draw();
						ObstaclesManager::draw();
					}
					break;
				case 2:
					Player::draw();
					ObstaclesManager::draw();
					break;
				}
			}
			else {
				ObstaclesManager::draw();
				Player::draw();
			}
		}
		//-----------------------------------------
		void draw() {
			//PARALLAX-----------------------
			DrawTextureEx(scene.backgroundLayer1, SCROLLBGLAYER1, 0.0f, 2.0f, WHITE);
			DrawTextureEx(scene.backgroundLayer1, POSBGLAYER1, 0.0f, 2.0f, WHITE);

			DrawTextureEx(scene.backgroundLayer2, SCROLLBGLAYER2, 0.0f, 2.0f, WHITE);
			DrawTextureEx(scene.backgroundLayer2, POSBGLAYER2, 0.0f, 2.0f, WHITE);

			DrawTextureEx(scene.backgroundLayer3, SCROLLBGLAYER3, 0.0f, 2.0f, WHITE);
			DrawTextureEx(scene.backgroundLayer3, POSBGLAYER3, 0.0f, 2.0f, WHITE);
			//OTHER-----------------------
			ObstaclesManager::drawRoofall();
			if (ObstaclesManager::obstacle2.layer != Player::guy.layer) {
				switch (ObstaclesManager::obstacle2.layer)
				{
				case 0:
					ObstaclesManager::draw();
					Player::draw();
					break;
				case 1:
					if (Player::guy.layer == 2) {
						ObstaclesManager::draw();
						Player::draw();
					}
					else if (Player::guy.layer == 0) {
						Player::draw();
						ObstaclesManager::draw();
					}
					break;
				case 2:
					Player::draw();
					ObstaclesManager::draw();
					break;
				}
			}
			else {
				if (!Player::guy.evading) {
					ObstaclesManager::draw();
					Player::draw();
				}
				else if (Player::guy.evading && ObstaclesManager::obstacle2.typeObs == ObstaclesManager::waterTank) {
					Player::draw();
					ObstaclesManager::draw();
				}
				else if (ObstaclesManager::obstacle2.typeObs != ObstaclesManager::waterTank) {
					ObstaclesManager::draw();
					Player::draw();
				}
			}

#if DEBUG
			DrawRectangleLinesEx(scene.floorLayer0, 2, ORANGE);
			DrawRectangleLinesEx(scene.floorLayer1, 2, BLUE);
			DrawRectangleLinesEx(scene.floorLayer2, 2, YELLOW);
#endif
		}
		//-----------------------------------------
		void resizeOnResolution() {
			scene.floorLayer0.y = static_cast<float>(toPoseLayer0);
			scene.floorLayer1.y = static_cast<float>(toPoseLayer1);
			scene.floorLayer2.y = static_cast<float>(toPoseLayer2);
			scene.floorLayer0.width = (float)actualWidth;
			scene.floorLayer1.width = (float)actualWidth;
			scene.floorLayer2.width = (float)actualWidth;
			loadTextures();
		}
		//-----------------------------------------
		void update() {
			if (!Player::guy.falling && Player::guy.alive) {
				scrollingBack -= (0.1f + ((ObstaclesManager::speedObstacles / 2) * GetFrameTime()));
				scrollingMid -= (0.5f + ((ObstaclesManager::speedObstacles / 2) * GetFrameTime()));
				scrollingFore -= (1.0f + ((ObstaclesManager::speedObstacles / 2) * GetFrameTime()));

				if (scrollingFore <= -scene.backgroundLayer3.width * 2) scrollingFore = 0;
				if (scrollingMid <= -scene.backgroundLayer2.width * 2) scrollingMid = 0;
				if (scrollingBack <= -scene.backgroundLayer1.width * 2) scrollingBack = 0;

				updateBgsPos();
			}
		}
		//-----------------------------------------
		void updateBgsPos() {
			SCROLLBGLAYER1 = { scrollingBack, 0.0f };
			SCROLLBGLAYER2 = { scrollingMid , 0.0f };
			SCROLLBGLAYER3 = { scrollingFore, 0.0f };
			POSBGLAYER1 = { scene.backgroundLayer1.width * 2 + scrollingBack,0.0f };
			POSBGLAYER2 = { scene.backgroundLayer2.width * 2 + scrollingMid ,0.0f };
			POSBGLAYER3 = { scene.backgroundLayer3.width * 2 + scrollingFore,0.0f };
		}
		//-----------------------------------------
		void deinit() {
			unloadTextures();
		}
		//-----------------------------------------
		void unloadTextures() {
			UnloadTexture(scene.backgroundLayer1);
			UnloadTexture(scene.backgroundLayer2);
			UnloadTexture(scene.backgroundLayer3);
		}
		//-----------------------------------------
	}
}