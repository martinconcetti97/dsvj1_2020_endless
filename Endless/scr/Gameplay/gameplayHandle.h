#ifndef GAMEPLAYHANDLE_H
#define GAMEPLAYHANDLE_H

namespace Endless {
	namespace GameManager {
		//-----------------
		enum GAME_STATE {
			RESUME,
			GOMENU
		};
		//-----------------
		extern GAME_STATE gameState;
		//-----------------
		extern bool onPause;
		extern bool selectThat;
		//-----------------
		extern bool onGameplay;

		void initGameElements();
		void inputsGame();
		void reinitOnDead();
		void loadTexturesGame();
		void drawGameElements();
		void handleKeys(int& keysToHandle);
		void updateGameElements();
		void deinitGameElements();
	}
}
#endif // !GAMEPLAYHANDLE_H