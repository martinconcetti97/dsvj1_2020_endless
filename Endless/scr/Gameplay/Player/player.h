#ifndef  PLAYER_H
#define PLAYER_H

#include "raylib.h"

#define MAX_FRAME_SPEED 18
#define MIN_FRAME_SPEED 1

namespace Endless {
	namespace Player {
		const int STATE_ANIM = 6;
		
		enum ANIMATIONS {
			RUNNING = 1,
			JUMPING,
			CRASHING,
			DEAD,
			FALLING,
			EVADING
		};
		
		extern ANIMATIONS anim;
		extern Texture2D scoreplayer;

		struct SOMEGUY {
			Rectangle collider;
			Texture2D player[STATE_ANIM];
			Rectangle frameRec;
			Vector2 TEXTURE_POS;
			float speed;
			float gravInfluence;
			int score;
			int layer;

			int ANIM_STATE;

			bool evading;
			bool alive;
			bool onGround;
			bool jumping;
			bool onChangeLayerDown;
			bool onChangeLayerUp;
			bool falling;
		};

		extern SOMEGUY guy;
		extern float WIDTH;
		extern float HEIGTH;

		void init();
		void inputs();
		void update();
		void draw();
		void deinit();
		void loadTexture();
		void unloadTexture();
		void applyPhysics();
		void makeTransAnimations();
		void resizeOnResolution();
	}
}
#endif // ! PLAYER_H