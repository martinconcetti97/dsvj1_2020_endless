#include <iostream>

#include "screen.h"

#include "Gameplay/Scene/scene.h"
#include "Gameplay/Player/player.h"
#include "Gameplay/Obstacles/obstacles.h"
#include "Menu/Buttons/buttons.h"
#include "Menu/menuHandeling.h"

using namespace Endless;

namespace Endless {
	namespace ScreenHandle {

		bool onRezise;
		//-----------------------------------
		int maxFPS = 120;
		int actualWidth;
		int actualHeigth;
		RESOLUTIONS chooseWindow;
		int defaultWidth = chooseWindow.highWidth;
		int defaultHeigth = chooseWindow.highHeigth;
		//--------------------------------------------------
		float toPoseLayer0;
		float toPoseLayer1;
		float toPoseLayer2;
		float posRoof;
		//--------------------------------------------------
		void init() {
			//-------------------
			actualWidth = defaultWidth;
			actualHeigth = defaultHeigth;
			InitWindow(actualWidth, actualHeigth, "Endless - Game - v0.1.9");
			//-------------------
			posRoof = static_cast<float>(actualHeigth / 1.5f);
			toPoseLayer0 = static_cast<float>(((1080 / 1.5f) * chooseWindow.highHeigth) / (1080));
			toPoseLayer1 = static_cast<float>(toPoseLayer0 + 50);
			toPoseLayer2 = static_cast<float>(toPoseLayer0 + 100);
			//-------------------
			SetTargetFPS(maxFPS);
			onRezise = false;
			SetExitKey(KEY_VOLUME_UP);
		}
		//--------------------------------------------------
		void setWindowResolution(int choose)
		{	/*1 LOW, 2 MEDIUM, 3 HIGH, 4 DEFAULT(MEDIUM)*/
			switch (choose)
			{
			case 1:
				SetWindowSize(chooseWindow.lowWidth, chooseWindow.lowHeigth);
				actualWidth = chooseWindow.lowWidth;
				actualHeigth = chooseWindow.lowHeigth;
				posRoof = static_cast<float>(actualHeigth / 1.5f);
				toPoseLayer0 = static_cast<float>((posRoof * chooseWindow.lowHeigth) / actualHeigth);
				toPoseLayer1 = static_cast<float>(toPoseLayer0 + 50);
				toPoseLayer2 = static_cast<float>(toPoseLayer0 + 100);
				break;
			case 2:
				SetWindowSize(chooseWindow.mediumWidth, chooseWindow.mediumHeigth);
				actualWidth = chooseWindow.mediumWidth;
				actualHeigth = chooseWindow.mediumHeigth;
				posRoof = static_cast<float>(actualHeigth / 1.5f);
				toPoseLayer0 = static_cast<float>((posRoof * chooseWindow.mediumHeigth) / actualHeigth);
				toPoseLayer1 = static_cast<float>(toPoseLayer0 + 50);
				toPoseLayer2 = static_cast<float>(toPoseLayer0 + 100);
				break;
			case 3:
				SetWindowSize(chooseWindow.highWidth, chooseWindow.highHeigth);
				actualWidth = chooseWindow.highWidth;
				actualHeigth = chooseWindow.highHeigth;
				posRoof = static_cast<float>(actualHeigth / 1.5f);
				toPoseLayer0 = static_cast<float>((posRoof * chooseWindow.highHeigth) / actualHeigth);
				toPoseLayer1 = static_cast<float>(toPoseLayer0 + 50);
				toPoseLayer2 = static_cast<float>(toPoseLayer0 + 100);
				break;
			case 4:
				SetWindowSize(defaultWidth, defaultHeigth);
				actualWidth = defaultWidth;
				actualHeigth = defaultHeigth;
				break;
			default:
				std::cout << "ERROR01:Fallo el switch de cambio de resoluciones" << std::endl;
				break;
			}
			//Rescale all the textures
			std::cout << toPoseLayer0 << std::endl;
			std::cout << toPoseLayer1 << std::endl;
			std::cout << toPoseLayer2 << std::endl;
			onRezise = true;
			SceneManagerGame::resizeOnResolution();
		}
		//--------------------------------------------------
		void changeResolution(int resToChange) {
			ScreenHandle::setWindowResolution(resToChange);
			Player::resizeOnResolution();
			SceneManagerMenu::reziseOnResolu();
			ButtonHandle::resizeOnResolution();
			ObstaclesManager::reziseOnResolution();
		}
		//--------------------------------------------------
		/*
		if (IsKeyPressed(KEY_R)) {
				ScreenHandle::setWindowResolution(changeRes);
				Player::resizeOnResolution();
				if (changeRes >= 1)
					changeRes++;
				SceneManagerMenu::reziseOnResolu();
				ButtonHandle::resizeOnResolution();
				ObstaclesManager::reziseOnResolution();
			}
			if (changeRes > 4)
				changeRes = 1;
		*/
	}
}