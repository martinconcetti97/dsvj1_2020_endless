#include "obstacles.h"

#include "Gameplay/Screen/screen.h"
#include "Gameplay/Scene/scene.h"
#include "Gameplay/Player/player.h"

using namespace Endless;
using namespace ScreenHandle;
using namespace Player;

namespace Endless {
	namespace ObstaclesManager {
		//------------------------------
		OBSTACLES obstacle1;
		OBSTACLES obstacle2;
		TYPE typeObstacle;
		//------------------------------
		static int randomType = 0;
		static int randomType2 = 0;
		static float randomX = 0;
		static float randomX2 = 0;
		static int randomToChooseY = 0;
		static int randomToChooseY2 = 0;
		static float randomYObs1 = 0;
		static float randomYObs2 = 0;
		static float randomYObs3 = 0;

		static float heigthRoof = 0;
		static float heigthPipe = 0;
		static float heigthWaterTank = 0;
		static float ROOFFALLtextureY = 0.0f;

		static float randPosY0 = 0;
		static float randPosY1 = 0;
		static float randPosY2 = 0;
		//------------------------------
		float speedObstacles;
		//------------------------------
		Color colorType;
		Color colorType2;
		//------------------------------
		bool alreadyLoaded = false;
		//------------------------------
		Texture2D auxTextureObstacle2[amountObstacles];
		//------------------------------
		void initObstacle() {
			speedObstacles = 250.0f;
			heigthRoof = (float)(toPoseLayer0);
			heigthPipe = (float)(HEIGTH / 2);
			heigthWaterTank = (float)(HEIGTH * 1.3);
			obstacle1.badThing.height = heigthRoof;
			randPosY0 = static_cast<float>(toPoseLayer0);
			randPosY1 = static_cast<float>(toPoseLayer1);
			randPosY2 = static_cast<float>(toPoseLayer2);
			colorType = WHITE;
			colorType2 = WHITE;
			obstacle1.isPlaced = false;
			if (!alreadyLoaded)
				loadTextures();
			ROOFFALLtextureY = toPoseLayer0 - 8;
		}
		//------------------------------
		void genObstacles() {
			if (!obstacle1.isPlaced) {
				randomType = 1;
				randomX = (float)GetRandomValue(actualWidth, actualWidth + 100);
				randomToChooseY = 1;

				switch (randomToChooseY)
				{
				case 1:
					randomYObs1 = randPosY1;
					obstacle1.layer = 1;
					break;
				}

				switch (randomType)
				{
				case 1:
					obstacle1.typeObs = roofdrop;
					colorType = RED;
					obstacle1.badThing = { randomX,randPosY1 - 100,(float)(speedObstacles / 3),heigthRoof };
					obstacle1.badThingTexture.width = (int)(speedObstacles / 3);
					obstacle1.isPlaced = true;
					break;
				}
			}
			if (!obstacle2.isPlaced) {
				randomType2 = GetRandomValue(1, 2);
				randomX2 = (float)GetRandomValue(actualWidth + 100, actualWidth + 500);
				randomToChooseY2 = GetRandomValue(0, 2);

				switch (randomToChooseY2)
				{
				case 0:
					randomYObs2 = randPosY0;
					obstacle2.layer = 0;
					break;
				case 1:
					randomYObs2 = randPosY1;
					obstacle2.layer = 1;
					break;
				case 2:
					randomYObs2 = randPosY2;
					obstacle2.layer = 2;
					break;
				}

				switch (randomType2)
				{
				case 1:
					obstacle2.typeObs = pipe;
					colorType2 = ORANGE;
					obstacle2.badThing = { randomX2,randomYObs2 - heigthPipe,(float)(85),heigthPipe };
					obstacle2.badThingTexture.width = (int)(85);
					obstacle2.isPlaced = true;
					obstacle2.badThingTexture = auxTextureObstacle2[1];
					break;
				case 2:
					obstacle2.typeObs = waterTank;
					colorType2 = PINK;
					obstacle2.badThing = { randomX2,randomYObs2 - heigthWaterTank,(float)(75),heigthWaterTank };
					obstacle2.badThingTexture.width = (int)(80);
					obstacle2.isPlaced = true;
					obstacle2.badThingTexture = auxTextureObstacle2[0];
					break;
				}
			}
		}
		//------------------------------
		void update() {
			genObstacles();

			if (obstacle1.badThing.x <= 0 - obstacle1.badThing.width) {
				obstacle1.isPlaced = false;
			}
			else {
				if (!Player::guy.falling && Player::guy.alive)
					obstacle1.badThing.x -= ((speedObstacles)*GetFrameTime());
			}
			if (obstacle2.badThing.x <= 0 - obstacle2.badThing.width) {
				obstacle2.isPlaced = false;
			}
			else {
				if (!Player::guy.falling && Player::guy.alive)
					obstacle2.badThing.x -= ((speedObstacles)*GetFrameTime());
			}
		}
		//------------------------------
		void draw() {
			DrawTexture(obstacle2.badThingTexture, static_cast<int>(obstacle2.badThing.x), static_cast<int>(obstacle2.badThing.y), WHITE);
#if DEBUG
			DrawRectangleLinesEx(obstacle1.badThing, 3, colorType);
			DrawRectangleLinesEx(obstacle2.badThing, 3, colorType2);
#endif
		}
		//------------------------------
		void drawRoofall() {
			DrawTexture(obstacle1.badThingTexture, static_cast<int>(obstacle1.badThing.x), static_cast<int>(ROOFFALLtextureY), WHITE);
		}
		//------------------------------
		void reziseOnResolution() {
			randPosY0 = static_cast<float>(toPoseLayer0);
			randPosY1 = static_cast<float>(toPoseLayer1);
			randPosY2 = static_cast<float>(toPoseLayer2);
			heigthRoof = (float)(toPoseLayer0);
			obstacle1.badThing = { randomX,randPosY1 - 100,(float)(speedObstacles / 3),heigthRoof };
			ROOFFALLtextureY = toPoseLayer0 - 8;
			switch (randomType2)
			{
			case 1:
				obstacle2.badThing = { randomX2,randomYObs2 - heigthPipe,(float)(85),heigthPipe };
				break;
			case 2:
				obstacle2.badThing = { randomX2,randomYObs2 - heigthWaterTank,(float)(75),heigthWaterTank };
				break;
			}
			loadTextures();
		}
		//------------------------------
		void loadTextures() {
			//717 PIXELES DONDE EMPIEZA EL TECHO DEL BACKGROUND 3  en 1080 de alto
			//363 PIXELES BASES ROOFFALL A 1080 DE ALTO
			Image image;
			if (!onRezise) {
				//---------
				image = LoadImage("res/assets/obstacles/pit.png");
				ImageResize(&image, static_cast<int>(speedObstacles / 3), static_cast<int>(heigthRoof));
				obstacle1.badThingTexture = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/bigGrave.png");
				ImageResize(&image, static_cast<int>(75), static_cast<int>(heigthWaterTank));
				auxTextureObstacle2[0] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/small.png");
				ImageResize(&image, static_cast<int>(85), static_cast<int>(heigthPipe));
				auxTextureObstacle2[1] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
			}
			else {
				unloadTextures();
				//---------
				image = LoadImage("res/assets/obstacles/pit.png");
				ImageResize(&image, static_cast<int>(speedObstacles / 3), static_cast<int>(heigthRoof));
				obstacle1.badThingTexture = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/bigGrave.png");
				ImageResize(&image, static_cast<int>(75), static_cast<int>(heigthWaterTank));
				auxTextureObstacle2[0] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
				image = LoadImage("res/assets/obstacles/small.png");
				ImageResize(&image, static_cast<int>(85), static_cast<int>(heigthPipe));
				auxTextureObstacle2[1] = LoadTextureFromImage(image);
				UnloadImage(image);
				//---------
			}
			alreadyLoaded = true;
			onRezise = false;
		}
		//------------------------------
		void unloadTextures() {
			UnloadTexture(obstacle1.badThingTexture);
			UnloadTexture(auxTextureObstacle2[0]);
			UnloadTexture(auxTextureObstacle2[1]);
			UnloadTexture(obstacle2.badThingTexture);
		}
		//------------------------------

	}
}