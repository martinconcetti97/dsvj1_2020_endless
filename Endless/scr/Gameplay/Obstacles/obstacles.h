#ifndef OBSTACLES_H
#define OBSTACLES_H

#include "raylib.h"

namespace Endless {
	namespace ObstaclesManager {
		//------------------------------
		const int amountObstacles = 2;
		//------------------------------
		enum TYPE
		{
			roofdrop = 1,
			pipe,
			waterTank
		};
		//------------------------------
		extern TYPE typeObstacle;
		//------------------------------
		struct OBSTACLES
		{
			Rectangle badThing;
			Texture2D badThingTexture;
			TYPE typeObs;
			bool isPlaced;
			int layer;
		};
		//------------------------------
		extern OBSTACLES obstacle1;
		extern OBSTACLES obstacle2;
		//------------------------------
		extern float speedObstacles;

		void initObstacle();
		void genObstacles();
		void update();
		void draw();
		void drawRoofall();
		void draw();
		void reziseOnResolution();
		void loadTextures();
		void unloadTextures();
	}
}
#endif // !OBSTACLES_H