#include <iostream>

#include "gameplayHandle.h"

#include "Gameplay/Scene/scene.h"
#include "Gameplay/Obstacles/obstacles.h"
#include "Gameplay/Player/player.h"
#include "Gameplay/Sfx/Music/music.h"
#include "Gameplay/Sfx/Sounds/sounds.h"
#include "Menu/Buttons/buttons.h"
#include "Menu/menuHandeling.h"

using namespace Endless;

namespace Endless {
	namespace GameManager {
		//-------------------------------
		static int pauseKey = 0;

		//-------------------------------
		bool onPause;		
		bool selectThat;
		//-------------------------------
		GAME_STATE gameState;
		bool onGameplay;

		//-------------------------------
		void initGameElements() {
			gameState = GOMENU;
			//-------------
			onGameplay = true;

			SceneManagerGame::init();
			//-------------
			Player::init();
			//-------------
			ObstaclesManager::initObstacle();
			//-------------
		}
		//-------------------------------
		void inputsGame() {
			//-------------
			Player::inputs();
			//-------------
		}
		//-------------------------------
		void reinitOnDead() {
			//-------------
			Player::init();
			//-------------
			ObstaclesManager::initObstacle();
			//-------------
		}
		//-------------------------------
		void loadTexturesGame() {
			//-------------
			//-------------
		}
		//-------------------------------
		void drawGameElements() {
			//-------------
			if (!onPause) {
				SceneManagerGame::draw();
			}
			else {
				SceneManagerGame::drawLastFrame();
				ButtonHandle::draw_pause();
			}
			//-------------
		}
		//-------------------------------
		void handleKeys(int& keysToHandle) {
			//-----------------------------
			if (IsKeyPressed(KEY_DOWN)) {
				if (keysToHandle <= GOMENU) {
					keysToHandle++;
				}
			}
			else if (IsKeyPressed(KEY_UP)) {
				if (keysToHandle >= RESUME) {
					keysToHandle--;
				}
			}
			if (keysToHandle < RESUME)keysToHandle = GOMENU;
			else if (keysToHandle > GOMENU)keysToHandle = RESUME;
			//-----------------------------
			if (IsKeyPressed(KEY_ENTER))
				selectThat = true;
			//-----------------------------
		}
		//-------------------------------
		static void resumeOption() {
			ButtonHandle::RESUME.state = true;
			ButtonHandle::GOMENU.state = false;
			if (selectThat) {
				onPause = false;
				selectThat = false;
			}
		}
		//-------------------------------
		static void gomenuOption() {
			ButtonHandle::RESUME.state = false;
			ButtonHandle::GOMENU.state = true;
			if (selectThat) {
				onGameplay = false;
				MenuHanlde::onMenu = true;
				selectThat = false;
				reinitOnDead();
				onPause = false;
			}
		}
		//-------------------------------
		void updateGameElements() {
			//-------------
			if (!onPause) {
				//-------------
				SceneManagerGame::update();
				//-------------
				Player::update();
				//-------------
				ObstaclesManager::update();
				//-------------
			}
			else {
				handleKeys(pauseKey);

				switch (pauseKey)
				{
				case Endless::GameManager::RESUME:	resumeOption();
					break;
				case Endless::GameManager::GOMENU:	gomenuOption();
					break;
				default:
					std::cout << "ERROR06: Fallo switch gameplayHanlde draw!" << std::endl;
					break;
				}
			}
			//-------------
		}
		//-------------------------------
		void deinitGameElements() {
			//-------------
			Player::deinit();
			//-------------
			SceneManagerGame::deinit();
			//-------------
			ObstaclesManager::unloadTextures();
			//-------------
		}
		//-------------------------------
	}
}