#ifndef MENUHANDELING_H
#define MENUHANDELING_H

#include "Menu/Background/scenes.h"
#include "Menu/Buttons/buttons.h"
#include "Menu/Sfx/ButtonSounds/soundsMenu.h"
#include "Menu/Sfx/MenuMusic/musicMenu.h"

namespace Endless {
	namespace MenuHanlde {
		//------------------------
		enum MENU_STATE {
			MAINMENU,
			OPTIONSMENU,
			CREDITSMENU,
		};
		//------------------------
		enum MAIN_MENU {
			START,
			OPTIONS,
			CREDITS,
			EXIT
		};
		//------------------------
		enum OPTIONS_MENU {
			HOWPLAY,
			MUSICSETT,
			CHANGERES,
			BACK
		};
		//------------------------
		enum RESOL_MENU {
			_800x600,
			_1080x720,
			_1440x900
		};
		//------------------------
		extern MENU_STATE menustate;
		//------------------------
		extern MAIN_MENU mmenu;
		//------------------------
		extern OPTIONS_MENU omenu;
		//------------------------
		extern RESOL_MENU resmenu;
		//------------------------
		extern bool onMenu;
		extern bool selectThat;
		//------------------------

		void init();
		void loadAll();
		void draw();
		void update();
		void handleKeys(int& keysToHandle, int minState, int maxState);
		void deinit();
		void unload();
	}
}
#endif // !MENUHANDELING_H