#include <iostream>

#include "buttons.h"

#include "Gameplay/Screen/screen.h"
#include "Gameplay/gameplayHandle.h"

using namespace Endless;
using namespace ScreenHandle;

namespace Endless {
	namespace ButtonHandle {
		//-----------------------------------
		Rectangle pauseBlind;
		//-----------------------------------
			//-----------------------
			//MENU
		BUTTONS START;
		BUTTONS OPTIONS;
		BUTTONS CREDITS;
		BUTTONS EXIT;
		//-----------------------
		//GAMEPLAY
		BUTTONS GOMENU;
		BUTTONS RESUME;
		//-----------------------
		//OPTIONS
		BUTTONS HELP;
		BUTTONS MUSIC;
		BUTTONS SCREEN;
		BUTTONS BACK;
		//-----------------------
		BUTTONS _800X600;
		BUTTONS _1080X720;
		BUTTONS _1440X900;
		//-----------------------------------
		void init() {
			//-----------------------------
			START.POS = { static_cast<float>((actualWidth / 1.4f) - (START.BTN->width / 2)),   static_cast<float>(actualHeigth / 6) };
			OPTIONS.POS = { static_cast<float>((actualWidth / 1.4f) - (OPTIONS.BTN->width / 2)), static_cast<float>(actualHeigth / 3) };
			CREDITS.POS = { static_cast<float>((actualWidth / 1.4f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 2) };
			EXIT.POS = { static_cast<float>((actualWidth / 1.4f) - (EXIT.BTN->width / 2)),	static_cast<float>(actualHeigth / 1.5f) };
			//-----------------------------
			HELP.POS = { static_cast<float>((actualWidth / 1.4f) - (START.BTN->width / 2)),   static_cast<float>(actualHeigth / 6) };
			MUSIC.POS = { static_cast<float>((actualWidth / 1.4f) - (OPTIONS.BTN->width / 2)), static_cast<float>(actualHeigth / 3) };
			SCREEN.POS = { static_cast<float>((actualWidth / 1.4f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 2) };
			BACK.POS = { static_cast<float>((actualWidth / 1.4f) - (EXIT.BTN->width / 2)),	static_cast<float>(actualHeigth / 1.5f) };
			//-----------------------------
			RESUME.POS = { static_cast<float>((actualWidth / 2) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 3) };
			GOMENU.POS = { static_cast<float>((actualWidth / 2) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 2) };
			//-----------------------------
			_800X600.POS = { static_cast<float>((actualWidth / 1.9f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 2) };
			_1080X720.POS = { static_cast<float>((actualWidth / 1.9f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 1.5f) };
			_1440X900.POS = { static_cast<float>((actualWidth / 1.9f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 1.2f) };
			//-----------------------------
			START.state = true;
			OPTIONS.state = false;
			CREDITS.state = false;
			EXIT.state = false;
			//-----------------------
			HELP.state = false;
			MUSIC.state = false;
			SCREEN.state = false;
			BACK.state = false;
			//-----------------------
			RESUME.state = false;
			GOMENU.state = false;
			//-----------------------------
			pauseBlind = { 0.0f,0.0f,static_cast<float>(actualWidth),static_cast<float>(actualHeigth) };
			//-----------------------------
			GameManager::gameState = GameManager::GOMENU;
			//-----------------------------
		}
		//-----------------------------------
		void load() {
			//-----------------------------------
			Image reSize[state];
			//-----------------------------------
			if (!onRezise) {
				//-----------------------------------5 NUMERO HERMOSO junto al 8
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonStart%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					START.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonOptions%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					OPTIONS.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonCredits%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					CREDITS.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonExit%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					EXIT.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonGoMenu%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					GOMENU.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonResume%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					RESUME.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonHelp%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					HELP.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonMusic%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					MUSIC.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonScreen%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					SCREEN.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonBack%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					BACK.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonResLow%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					_800X600.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonResMid%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					_1080X720.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonResHig%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					_1440X900.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
			}
			else {
				//-----------------------------
				unload();
				//-----------------------------------5 NUMERO HERMOSO junto al 8
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonStart%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					START.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonOptions%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					OPTIONS.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonCredits%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					CREDITS.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonExit%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					EXIT.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonGoMenu%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					GOMENU.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonResume%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					RESUME.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonHelp%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					HELP.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonMusic%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					MUSIC.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonScreen%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					SCREEN.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonBack%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					BACK.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonResLow%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					_800X600.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonResMid%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					_1080X720.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
				for (int i = 0; i < state; i++) {
					reSize[i] = LoadImage(FormatText("res/assets/buttons/buttonResHig%i.png", i));
					ImageResize(&reSize[i], static_cast<int>(actualWidth / 5), static_cast<int>(actualHeigth / 8));
					_1440X900.BTN[i] = LoadTextureFromImage(reSize[i]);
					UnloadImage(reSize[i]);
				}
				//-----------------------------------
			}
		}
		//----------------------------------- MMENU (MAIN MENU)
		void draw_mmenu() {
			//--------------
			DrawTextureEx(START.BTN[static_cast<int>(START.state)], START.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(OPTIONS.BTN[static_cast<int>(OPTIONS.state)], OPTIONS.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(CREDITS.BTN[static_cast<int>(CREDITS.state)], CREDITS.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(EXIT.BTN[static_cast<int>(EXIT.state)], EXIT.POS, 0.0f, 1.0f, WHITE);
			//--------------
		}
		//-----------------------------------
		void draw_optionsmenu() {
			//--------------
			DrawTextureEx(HELP.BTN[static_cast<int>(HELP.state)], HELP.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(MUSIC.BTN[static_cast<int>(MUSIC.state)], MUSIC.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(SCREEN.BTN[static_cast<int>(SCREEN.state)], SCREEN.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(BACK.BTN[static_cast<int>(BACK.state)], BACK.POS, 0.0f, 1.0f, WHITE);
			//--------------
		}
		//-----------------------------------
		void draw_credits() {

		}
		//-----------------------------------
		void draw_pause() {
			//--------------
			DrawRectangleRec(pauseBlind, PAUSEBLACK);
			DrawTextureEx(RESUME.BTN[static_cast<int>(RESUME.state)], RESUME.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(GOMENU.BTN[static_cast<int>(GOMENU.state)], GOMENU.POS, 0.0f, 1.0f, WHITE);
			//--------------
		}
		//-----------------------------------
		void draw_resolutions() {
			//--------------
			DrawTextureEx(_800X600.BTN[static_cast<int>(_800X600.state)], _800X600.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(_1080X720.BTN[static_cast<int>(_1080X720.state)], _1080X720.POS, 0.0f, 1.0f, WHITE);
			DrawTextureEx(_1440X900.BTN[static_cast<int>(_1440X900.state)], _1440X900.POS, 0.0f, 1.0f, WHITE);
			//--------------
		}
		//-----------------------------------
		void update() {

		}
		//-----------------------------------
		void resizeOnResolution() {
			load();
			//-----------------------------------
			START.POS = { static_cast<float>((actualWidth / 1.4f) - (START.BTN->width / 2)),   static_cast<float>(actualHeigth / 6) };
			OPTIONS.POS = { static_cast<float>((actualWidth / 1.4f) - (OPTIONS.BTN->width / 2)), static_cast<float>(actualHeigth / 3) };
			CREDITS.POS = { static_cast<float>((actualWidth / 1.4f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 2) };
			EXIT.POS = { static_cast<float>((actualWidth / 1.4f) - (EXIT.BTN->width / 2)),	static_cast<float>(actualHeigth / 1.5) };
			//-----------------------------------
			HELP.POS = { static_cast<float>((actualWidth / 1.4f) - (START.BTN->width / 2)),   static_cast<float>(actualHeigth / 6) };
			MUSIC.POS = { static_cast<float>((actualWidth / 1.4f) - (OPTIONS.BTN->width / 2)), static_cast<float>(actualHeigth / 3) };
			SCREEN.POS = { static_cast<float>((actualWidth / 1.4f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 2) };
			BACK.POS = { static_cast<float>((actualWidth / 1.4f) - (EXIT.BTN->width / 2)),	static_cast<float>(actualHeigth / 1.5) };
			//-----------------------------------
			RESUME.POS = { static_cast<float>((actualWidth / 2) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 3) };
			GOMENU.POS = { static_cast<float>((actualWidth / 2) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 2) };
			//-----------------------------
			_800X600.POS = { static_cast<float>((actualWidth / 1.9f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 2) };
			_1080X720.POS = { static_cast<float>((actualWidth / 1.9f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 1.5f) };
			_1440X900.POS = { static_cast<float>((actualWidth / 1.9f) - (CREDITS.BTN->width / 2)), static_cast<float>(actualHeigth / 1.2f) };
			//-----------------------------
			pauseBlind = { 0.0f,0.0f,static_cast<float>(actualWidth),static_cast<float>(actualHeigth) };
		}
		//-----------------------------------
		void deinit() {

		}
		//-----------------------------------
		void unload() {
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(START.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(OPTIONS.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(CREDITS.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(EXIT.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(GOMENU.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(RESUME.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(HELP.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(MUSIC.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(SCREEN.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(BACK.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(_800X600.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(_1080X720.BTN[i]); }
			//-------------
			for (int i = 0; i < state; i++) { UnloadTexture(_1440X900.BTN[i]); }
			//-------------
		}
		//-----------------------------------
	}
}