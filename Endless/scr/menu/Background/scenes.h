#ifndef SCENES_H
#define SCENES_H

#include "raylib.h"

namespace Endless {
	namespace SceneManagerMenu {
		//----------------------
		struct MENU_THINGS {
			Texture2D background;
			Texture2D credits;
			Texture2D help;
			Vector2 posBackground;
		};
		//----------------------
		extern MENU_THINGS menu;
		//----------------------
		void init();
		void load();
		void unload();
		void draw();
		void drawCredits();
		void drawHelp();
		void reziseOnResolu();
	}
}
#endif // !SCENES_H