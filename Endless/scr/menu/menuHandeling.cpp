#include <iostream>

#include "menuHandeling.h"

#include "Gameplay/Screen/screen.h"
#include "gameLoop.h"

using namespace Endless;

namespace Endless {
	namespace MenuHanlde {
		//-------------------------
		MENU_STATE	 menustate;
		//-------------------------
		MAIN_MENU	 mmenu;
		//-------------------------
		OPTIONS_MENU omenu;
		//-------------------------
		RESOL_MENU resmenu;
		//-------------------------
		static int mmenuKey = 0;
		static int omenuKey = 0;
		static int onresKey = 0;
		//-------------------------
		bool onMenu;
		bool selectThat;
		bool onChangeRes;
		bool onShowCredits;
		bool onShowHelp;
		//-------------------------
		int enterChangeRes = 1;
		int enterCredits = 1;
		int enterHelp = 1;
		//-------------------------

		//----------------------------------------------
		void init() {
			//---------
			menustate = MAINMENU;
			mmenu = START;
			omenu = HOWPLAY;
			//---------
			onMenu = true;
			selectThat = false;
			onChangeRes = false;
			onShowCredits = false;
			onShowHelp = false;
			//---------
			loadAll();
			//---------
			ButtonHandle::init();
			//---------
		}
		//----------------------------------------------
		void loadAll() {
			//----------------
			SceneManagerMenu::load();
			//----------------
			ButtonHandle::load();
			//----------------
		}
		//----------------------------------------------
		void draw() {
			//-----------------
			SceneManagerMenu::draw();
			if (!onShowCredits && !onShowHelp) {
				//-----------------
				switch (menustate)
				{
				case Endless::MenuHanlde::MAINMENU:		ButtonHandle::draw_mmenu();
					break;
				case Endless::MenuHanlde::OPTIONSMENU:	ButtonHandle::draw_optionsmenu();
					break;
				case Endless::MenuHanlde::CREDITSMENU:
					break;
				default:
					std::cout << "ERROR04: Fallo el switch de que draw!" << std::endl;
					break;
				}
				//-----------------
			}
			else if (onShowCredits) {
				SceneManagerMenu::drawCredits();
				if (IsKeyPressed(KEY_ENTER) && enterCredits == 0) {
					onShowCredits = false;
				}
				if (onShowCredits)
					enterCredits = 0;
				else
					enterCredits = 1;
			}
			else if (onShowHelp) {
				SceneManagerMenu::drawHelp();
				if (IsKeyPressed(KEY_ENTER) && enterHelp == 0) {
					onShowHelp = false;
				}
				if (onShowHelp)
					enterHelp = 0;
				else
					enterHelp = 1;
			}
			if (onChangeRes) ButtonHandle::draw_resolutions();
			//-----------------
		}
		//----------------------------------------------
		static void startGame() {
			mmenu = START;
			ButtonHandle::EXIT.state = false;
			ButtonHandle::START.state = true;
			ButtonHandle::OPTIONS.state = false;
			if (selectThat) {
				onMenu = false;
				selectThat = false;
			}
		}
		//----------------------------------------------
		static void options() {
			mmenu = OPTIONS;
			ButtonHandle::START.state = false;
			ButtonHandle::OPTIONS.state = true;
			ButtonHandle::CREDITS.state = false;
			if (selectThat) {
				menustate = OPTIONSMENU;
				selectThat = false;
			}
		}
		//----------------------------------------------
		static void credits() {
			mmenu = CREDITS;
			ButtonHandle::OPTIONS.state = false;
			ButtonHandle::CREDITS.state = true;
			ButtonHandle::EXIT.state = false;
			if (selectThat) {
				onShowCredits = true;
				selectThat = false;
				//enterCredits = 1;
			}
		}
		//----------------------------------------------
		static void exit() {
			mmenu = EXIT;
			ButtonHandle::CREDITS.state = false;
			ButtonHandle::EXIT.state = true;
			ButtonHandle::START.state = false;
			if (selectThat) {
				GameLoop::inGame = false;
				selectThat = false;
			}
		}
		//----------------------------------------------
		static void howToPlay() {
			ButtonHandle::BACK.state = false;
			ButtonHandle::HELP.state = true;
			ButtonHandle::MUSIC.state = false;
			if (selectThat) {
				onShowHelp = true;
				selectThat = false;
			}
		}
		//----------------------------------------------
		static void musicSet() {
			ButtonHandle::HELP.state = false;
			ButtonHandle::MUSIC.state = true;
			ButtonHandle::SCREEN.state = false;
		}
		//----------------------------------------------
		//--------------
		static void res800X600() {
			ButtonHandle::_1440X900.state = false;
			ButtonHandle::_800X600.state = true;
			ButtonHandle::_1080X720.state = false;
			if (IsKeyPressed(KEY_ENTER) && enterChangeRes == 0) {
				ScreenHandle::changeResolution(1);
			}
			enterChangeRes = 0;
			if (IsKeyPressed(KEY_RIGHT)) {
				onChangeRes = false;
				enterChangeRes = 1;
			}
		}
		//--------------
		static void res1080X720() {
			ButtonHandle::_800X600.state = false;
			ButtonHandle::_1080X720.state = true;
			ButtonHandle::_1440X900.state = false;
			if (IsKeyPressed(KEY_ENTER) && enterChangeRes == 0) {
				ScreenHandle::changeResolution(2);
			}
			enterChangeRes = 0;
			if (IsKeyPressed(KEY_RIGHT)) {
				onChangeRes = false;
				enterChangeRes = 1;
			}
		}
		//--------------
		static void res1440X900() {
			ButtonHandle::_1080X720.state = false;
			ButtonHandle::_1440X900.state = true;
			ButtonHandle::_800X600.state = false;
			if (IsKeyPressed(KEY_ENTER) && enterChangeRes == 0) {
				ScreenHandle::changeResolution(3);
			}
			enterChangeRes = 0;
			if (IsKeyPressed(KEY_RIGHT)) {
				onChangeRes = false;
				enterChangeRes = 1;
			}
		}
		//----------------------------------------------
		static void changeRes() {
			ButtonHandle::MUSIC.state = false;
			ButtonHandle::SCREEN.state = true;
			ButtonHandle::BACK.state = false;
			if (selectThat) {
				onChangeRes = true;
				selectThat = false;
			}
		}
		//----------------------------------------------
		static void back() {
			ButtonHandle::SCREEN.state = false;
			ButtonHandle::BACK.state = true;
			ButtonHandle::HELP.state = false;
			if (selectThat) {
				menustate = MAINMENU;
				selectThat = false;
			}
		}
		//----------------------------------------------
		static void resolutionUpdateBtns() {
			if (onChangeRes) {
				//-------
				handleKeys(onresKey, _800x600, _1440x900);
				//-------
				switch (onresKey)
				{
				case Endless::MenuHanlde::_800x600:		res800X600();
					break;
				case Endless::MenuHanlde::_1080x720:	res1080X720();
					break;
				case Endless::MenuHanlde::_1440x900:	res1440X900();
					break;
				default:
					std::cout << "ERROR07: Fallo el switch del resize en menuhablde!" << std::endl;
					break;
				}
				//-------
			}
		}
		//----------------------------------------------
		void update() {

			switch (menustate)
			{
			case Endless::MenuHanlde::MAINMENU:
				//-----------------------------
				if (!onChangeRes && !onShowCredits && !onShowHelp) { handleKeys(mmenuKey, START, EXIT); }
				//-----------------------------
						//-----------------------------
				switch (mmenuKey)
				{
				case START:	  /*-->*/	startGame();
					break;
				case OPTIONS: /*-->*/	options();
					break;
				case CREDITS: /*-->*/	credits();
					break;
				case EXIT:	  /*-->*/	exit();
					break;
				}
				//-----------------------------
				break;
			case Endless::MenuHanlde::OPTIONSMENU:
				//-----------------------------
				if (!onChangeRes && !onShowCredits && !onShowHelp) { handleKeys(omenuKey, HOWPLAY, BACK); }
				//-----------------------------
						//-----------------------------
				switch (omenuKey)
				{
				case HOWPLAY:	/*-->*/		howToPlay();
					break;
				case MUSICSETT: /*-->*/		musicSet();
					break;
				case CHANGERES: /*-->*/		changeRes();
					break;
				case BACK:		/*-->*/		back();
					break;
				}
				//-----------------------------
				break;
			case Endless::MenuHanlde::CREDITSMENU:
				break;
			default:
				std::cout << "ERROR05: Fallo swithc update menuHandle!" << std::endl;
				break;
			}
			if (onChangeRes)
				resolutionUpdateBtns();
		}
		//----------------------------------------------
		void handleKeys(int& keysToHandle, int minState, int maxState) {
			//-----------------------------
			if (IsKeyPressed(KEY_DOWN)) {
				if (keysToHandle <= maxState) {
					keysToHandle++;
				}
			}
			else if (IsKeyPressed(KEY_UP)) {
				if (keysToHandle >= minState) {
					keysToHandle--;
				}
			}
			if (keysToHandle < minState)keysToHandle = maxState;
			else if (keysToHandle > maxState)keysToHandle = minState;
			//-----------------------------
			if (IsKeyPressed(KEY_ENTER))
				selectThat = true;
			//-----------------------------
		}
		//----------------------------------------------
		void deinit() {
			unload();
		}
		//----------------------------------------------
		void unload() {
			//-------------
			SceneManagerMenu::unload();
			//-------------
			ButtonHandle::unload();
			//-------------
		}
		//----------------------------------------------
	}
}